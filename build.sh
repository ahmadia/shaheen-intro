NAME=index
python rst-directive.py \
    --stylesheet=pygments.css \
    --theme-url=ui/small-white \
    ${NAME}.rst > ${NAME}.html
