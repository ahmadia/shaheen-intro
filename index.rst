
Getting Started with Shaheen and Noor
================================================================================================

Shaheen
----------------------------

Shaheen is the Arabic word (شاهين ) for Peregrine Falcon 

.. list-table::
   :class: borderless

   * - Capable of reaching speeds of over 320 km/h in a stoop, the peregrine falcon is the fastest creature on the planet.
     - .. image:: media/peregrine_falcon.png
          :width: 90 %
          :alt: Peregrine Falcon
          :align: right

Blue Gene/P Architecture
----------------------------
.. image:: media/shaheen_layout.png
  :width: 90 %
  :alt: Shaheen Layout
  :align: center

Noor
----------------------------

Noor is the Arabic word (نور) for Light

.. list-table::
   :class: borderless

   * - Though perhaps they should have named it Fluffy, as there are three "heads" to the machine general-purpose
       compute nodes, GPU-accelerated nodes, and SMP nodes.
     - .. image:: media/Fluffy.jpg
          :width: 90 %
          :alt: Fluffy
          :align: right

Noor Architecture
----------------------------

* Compute Nodes (76)

  * Intel Xeon X5570s @ 2.93 GHz
  * 8 cores, 48GB RAM, 50GB /tmp

* GPU Nodes

  * Intel Xeon X5570s with NVidia Tesla 1070 coprocessor

* SMP Nodes

  * IBM Power6s @ 4.2 GHz
  * 32 cores, 512GB RAM, 250 GB /tmp

Where to Start
----------------------------

Your first reference should be the user guide for each system.

.. _Noor User Guide: http://rcweb.kaust.edu.sa/KAUST/ResearchComputing/wiki/NoorGuide
.. _Shaheen User Guide: http://hpc.kaust.edu.sa/documentation/user_guide/

* `Noor User Guide`_
* `Shaheen User Guide`_


Logging on
---------------------

.. sourcecode:: bash

   ssh username@shaheen.hpc.kaust.edu.sa


Example Code (bash) 
-------------------
 
.. sourcecode:: bash

    NAME=index
    python rst-directive.py \
        --stylesheet=pygments.css \
        --theme-url=ui/small-black \
        ${NAME}.rst > ${NAME}.html

Credits
------------
.. _Pygments Integration: http://hackmap.blogspot.com/2009/10/rst2s5-with-syntax-highlighting.html 
.. _MathJax Integration:  http://stackoverflow.com/questions/3610551/math-in-restructuredtext-with-latex/6529094#6529094
.. _Fluffy: http://www.flickr.com/photos/danielsemper/2054491513/

* `Pygments Integration`_
* `MathJax Integration`_
* `Fluffy`_
* Noor Introduction/Material - Benoit Marchand, Reseach Computing

.. footer:: Aron Ahmadia, KAUST Supercomputing Laboratory http://hpc.kaust.edu.sa/

.. raw:: html

     <script type="text/x-mathjax-config">
     MathJax.Hub.Config({
     extensions: ["tex2jax.js"],
     jax: ["input/TeX", "output/HTML-CSS"],
     tex2jax: {
       inlineMath: [ ['$','$'], ["\\(","\\)"] ],
       displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
       processEscapes: true
     },
     "HTML-CSS": { availableFonts: ["TeX"] }
     });
     </script>
     <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>
